module.exports = arregloUsuarios = [
    { nombre: 'Miguel', apellido: 'Reina' },
    { nombre: 'Cristian', apellido: 'Jumbo' },
    { nombre: 'Oscar', apellido: 'Sambache' },
    { nombre: 'Cristian', apellido: 'Lara' },
    { nombre: 'Carlos', apellido: 'Gutierrez' },
    { nombre: 'Adrian', apellido: 'Alborada' },
    { nombre: 'Marco', apellido: 'Reina' },
    { nombre: 'Miguel', apellido: 'Gamboa' },
    { nombre: 'Maria', apellido: 'Rodriguez' },
    { nombre: 'Andrea', apellido: 'Naranjo' },
    { nombre: 'Stefania', apellido: 'Rodriguez' },
    { nombre: 'Cecilia', apellido: 'Alvarado' },
    { nombre: 'Paul', apellido: 'Lara' },
    { nombre: 'Katherine', apellido: 'Naranjo' },
    { nombre: 'Jorge', apellido: 'Carvajal' }
];