const paquetes = require('./paquetes.js');


module.exports.crearBuscarUsuarioCallback = (arregloUsuarios, itemBuscarCrear, callback) => {
    paquetes.buscar.buscarUsuarioCallback(arregloUsuarios, itemBuscarCrear, (resultadoBusqueda) => {
        if (resultadoBusqueda) {
            callback({
                mensaje: 'Usuario encontrado.',
                usuario: resultadoBusqueda
            });
        } else {
            paquetes.crear.crearUsuariosCallback(arregloUsuarios, itemBuscarCrear, (resultadoUsuarioCreado) => {
                callback({
                    mensaje: 'Usuario no encontrado, usuario creado.',
                    arregloUsuarios: resultadoUsuarioCreado.arregloUsuarios
                });
            })
        }
    })
};
/* 
module.exports.crearBuscarUsuarioPromesas = (arregloUsuarios, itemBuscarCrear ) => {
    new
} */

module.exports.crearBuscarUsuarioPromesas = (arregloUsuarios, objetoNombreApellido) => {
    return new Promise((resolve, reject) => {
        arregloUsuarios.forEach((valor, indice) => {
            let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido
            if (condicionBusqueda) {
                resolve({
                    info: `el usuario ${valor.nombre} ${valor.apellido} fue encontrado}`
                })
            }
        });
        reject({
            detalle: 'fue creado el usuario',
            indice: arregloUsuarios.push(objetoNombreApellido),
            info: arregloUsuarios[10]
        })
    })
}

