module.exports.buscarExpresionCallback = (arregloUsuarios, expresionABuscar, callback) => {
    let arregloUsuariosEncontrados = arregloUsuarios.map((usuario) => {
        let encontroConicidenciaNombre = usuario.nombre.toLowerCase().search(expresionABuscar.toLowerCase()) != -1;
        if (encontroConicidenciaNombre) {
            return usuario;
        }
    }).filter((usuario) => {
        return usuario != undefined
    })
    if (arregloUsuariosEncontrados.length === 0) {
        callback({
            mensaje: `No se ha encontrado ningun usuario con la  expresion "${expresionABuscar}".`,
            arregloUsuariosEncontrados
        });
    }
    else {
        callback({
            mensaje: `Se ha encontrado los siguientes usuarios con la  expresion "${expresionABuscar}".`,
            arregloUsuariosEncontrados
        });
    }
}

module.exports.buscarExpresionPromesa = (arregloUsuarios, expresionABuscar) => {
    return new Promise((resolve, reject) => {
        let arregloUsuariosEncontrados = arregloUsuarios.map((usuario) => {
            let encontroConicidenciaNombre = usuario.nombre.toLowerCase().search(expresionABuscar.toLowerCase()) != -1;
            if (encontroConicidenciaNombre) {
                return usuario
            }
        }).filter((usuario) => {
            return usuario != undefined
        })
        if (arregloUsuariosEncontrados.length === 0) {
            reject({
                mensaje: `No se ha encontrado ningun usuario con la  expresion "${expresionABuscar}".`,
                arregloUsuariosEncontrados: arregloUsuariosEncontrados
            });
        }
        else {
            resolve({
                mensaje: `Se ha encontrado los siguientes usuarios con la  expresion "${expresionABuscar}".`,
                arregloUsuariosEncontrados: arregloUsuariosEncontrados
            });
        }
    })
}
