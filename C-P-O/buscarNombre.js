module.exports.buscarUsuarioCallback = (arregloUsuarios, itemUsuario, callback) => {
    const itemEncontrar = arregloUsuarios.find((usuario) => {
        return itemUsuario.nombre === usuario.nombre;
    });
    if (itemEncontrar) {
        callback({
            mensaje: 'Usuario encontrado.',
            info: itemUsuario
        });
    } else {
        callback({
            mensaje: 'Usuario no encontrado.',
            info: itemUsuario
        });
    };
}

module.exports.buscarUsuarioPromesas = (arregloUsuarios, itemUsuario) => {
    return new Promise((resolve, reject) => {
        const itemEncontrar = arregloUsuarios.find((usuario) => {
            return itemUsuario.nombre === usuario.nombre;
        });
        if (itemEncontrar) {
            resolve({
                mensaje: 'Usuario encontrado.',
                info: itemUsuario
            })
        } else {
            reject({
                mensaje: 'Usuario no encontrado.',
                info: itemUsuario
            })
        }
    })
}