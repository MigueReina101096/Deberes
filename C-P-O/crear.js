module.exports.crearUsuariosCallback = (arregloUsuarios, itemUsuario, callback) => {
    arregloUsuarios.push(itemUsuario);
    callback({
        mensaje: 'Usuario creado.',
        informacion: arregloUsuarios
    });
};

module.exports.crearUsuariosPromesas = (arregloUsuarios, itemUsuario) => {
    arregloUsuarios.push(itemUsuario);
    const respuestaPromesaCrear = (resolve) => {
        resolve({
            mensaje: 'Usuario creado.',
            informacion: arregloUsuarios
        })
    };
    return new Promise(respuestaPromesaCrear);
};
