module.exports.eliminarUsuarioCallback = (arregloUsuarios, objetoNombreApellido, cb) => {
    arregloUsuarios.forEach((valor, indice) => {
        let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido;
        if (condicionBusqueda) {
            arregloUsuarios = arregloUsuarios.splice(indice)
            cb({
                eliminado: `El usuario ${objetoNombreApellido.nombre} ${objetoNombreApellido.apellido} fue eliminado.}`
            });
        }
    });
};

module.exports.eliminarUsuarioPromesas = (arregloUsuarios, objetoNombreApellido) => {
    return new Promise((resolve, reject) => {
        arregloUsuarios.forEach((valor, indice) => {
            let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido;
            if (condicionBusqueda) {
                arregloUsuarios = arregloUsuarios.splice(indice);
                resolve({
                    eliminado: `El usuario ${objetoNombreApellido.nombre} ${objetoNombreApellido.apellido} fue eliminado.}`
                });
            }
        });
    })
};
