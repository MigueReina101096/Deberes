const arregloUsuarios = require('./arregloUsuarios.js');
const itemsModificables = require('./itemsModificables.js');
const crear = require('./crear.js');
const buscar = require('./buscar.js');
const buscarNombre = require('./buscarNombre.js');
const buscarApellido = require('./buscarApellido.js');
const buscarCrear = require('./buscarCrear');
const eliminar = require('./eliminar.js');
const buscarExpresion = require('./buscarExpresion.js');

module.exports = {
    arregloUsuarios,
    itemsModificables,
    crear,
    buscar,
    buscarNombre,
    buscarApellido,
    buscarCrear,
    buscarExpresion,
    eliminar
};