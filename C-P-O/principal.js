const paquetes = require('./paquetes.js');

paquetes.crear.crearUsuariosCallback(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario, (respuesta) => {
    console.log(respuesta);
});
paquetes.crear.crearUsuariosPromesas(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario1)
    .then((resolve) => {
        console.log(resolve);
    });
paquetes.buscar.buscarUsuarioCallback(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario2, (respuesta) => {
    console.log(respuesta);
});
paquetes.buscar.buscarUsuarioPromesas(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario2)
    .then((resolve) => {
        console.log(resolve);
    })
    .catch((reject) => {
        console.log(reject);
    });

paquetes.buscarNombre.buscarUsuarioCallback(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario3, (respuesta) => {
    console.log(respuesta);
});

paquetes.buscarApellido.buscarUsuarioCallback(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario4, (respuesta) => {
    console.log(respuesta);
});

/* paquetes.buscarCrear.crearBuscarUsuarioCallbacks(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario, (resultado)=>{
    console.log(resultado);
}); */

paquetes.buscarExpresion.buscarExpresionCallback(paquetes.arregloUsuarios, paquetes.itemsModificables.expresion, (resultado) => {
    console.log(resultado);
});

paquetes.eliminar.eliminarUsuarioCallback(paquetes.arregloUsuarios, paquetes.itemsModificables.itemUsuario1, (resultado) => {
    console.log(resultado);
});