const arregloUsuarios = {
    nombre: 'Miguel',
    apellido: 'Reina',
    edad: 22
};
const atributos = Object.keys(arregloUsuarios);
console.log(atributos);
Object.defineProperty(arregloUsuarios, 'nombre', { value: 'No tiene' });
Object.defineProperty(arregloUsuarios, 'apellido', { value: 'No tiene' });
Object.defineProperty(arregloUsuarios, 'edad', { value: 'No tiene' });
console.log('Proceso modificacndo uno a uno los atributos del JSON es:', arregloUsuarios);

function modificacionInformacionJson(arregloUsuarios, atributos) {
    atributos.forEach((valor) => {
        if(valor === 'nombre'){
            Object.defineProperty(arregloUsuarios, valor, { value: 'No tiene' });
        } 
    });
    return arregloUsuarios;
}
console.log('Con una función el nuevo JSON es:', modificacionInformacionJson(arregloUsuarios, atributos));



const sumar = (a,b)=>{
    const a1 = a | 0;
    const b1 = b | 0;
    return a1+b1;
}

const sumar1 = (a=0,b=0)=>{
    return a+b;
}

console.log(sumar(2,4));
console.log(sumar1(3));