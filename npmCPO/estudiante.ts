import { Persona } from './persona';
export class Estudiante extends Persona {
    constructor(public nombre: string, public apellido: string, carrera: string) {
        super(nombre, apellido);
        carrera = carrera;
    };
};