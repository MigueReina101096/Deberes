export class Persona {
    constructor(protected nombre: string, protected apellido: string) {
        this.nombre = nombre;
        this.apellido = apellido;
    };
};
    // private _nombre: string;
    // private _apellido: string;
    // public edad: number;
    // constructor(nombre: string, apellido: string) {
    //     this._nombre = nombre;
    //     this._apellido = apellido;
    //     this.edad = edad;
    // }
    // get Nombre() {
    //     return this._nombre;
    // }
    // set Nombre(nombre: string) {
    //     this._nombre = nombre;
    // }
    // get Apellido() {
    //     return this._apellido;
    // }
    // set Apellido(apellido: string) {
    //     this._apellido = apellido;
    // }