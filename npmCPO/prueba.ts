import {sumar} from './calculador';
import {Estudiante} from './estudiante';
import {UsuarioEPN} from './usuarioEpn';
import {UsuarioModificable} from './usuario2';
import {usuarioInterfaz} from './usuarioInterface';

sumar(2, 4);
sumar(2);
sumar();
// console.log('Persona ', persona);
// console.log('Persona nueva ', nuevaPersona);
const estudiante = new Estudiante('Carlos', 'Gutierrez', 'Ingenieria en Sistemas');
console.log('Estudiante ', estudiante);
const epn = new UsuarioEPN('Miguel', 'Reina', 12315235623, '****');
console.log('Estudiante EPN: ', epn);
const supermaxi = new UsuarioModificable('Miguel', 'Reina', 12315235623);
console.log(supermaxi);
//Interfaces
console.log('Primer interface: ', usuarioInterfaz);
usuarioInterfaz.nombre = 'Cristian';
usuarioInterfaz.apellido = 'Jumbo';
console.log('Segundo interface: ', usuarioInterfaz);