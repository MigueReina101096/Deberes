export class UsuarioBase {
    constructor(protected nombre: string, protected apellido: string, protected ci: number) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ci = ci;
    };
};