export class UsuarioModificable {
    private _nombre: string;
    private _apellido: string;
    private _ci: number;
    constructor(nombre: string, apellido: string, ci: number) {
        this._nombre = nombre;
        this._apellido = apellido;
        this._ci = ci;
    };
    get nombre() {
        return this._nombre;
    };
    set nombre(nombre: string) {
        this._nombre = nombre;
    };
    get apellido() {
        return this._apellido;
    };
    set apellido(apellido: string) {
        this._apellido = apellido;
    };
    get ci() {
        return this._ci;
    };
    set ci(ci: number) {
        this._ci = ci;
    };
};