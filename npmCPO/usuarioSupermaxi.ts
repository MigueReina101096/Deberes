import { UsuarioModificable } from './usuario2'
export class UsuarioSupermaxi extends UsuarioModificable {
    constructor(public nombre: string, public apellido: string, public ci: number) {
        super(nombre, apellido, ci);
    };
};