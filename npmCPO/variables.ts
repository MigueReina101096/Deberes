export const persona: object = {
    nombre: 'Cristian',
    apellido: 'Lara'
};
export const edad: number = 55;
export const nombre: string = 'test';
export let numero: number;
export const estado: boolean = true;
export const arreglo: Array<number | string | boolean> = [];
export const arreglo2: number[] = [];
export let variableCualquiera: any;
export const persona1: object = {
    nombre: 'Miguel',
    apellido: 'Reina'
};
export const nuevaPersona: Persona = new Persona('Manuel', 'Trujillo');